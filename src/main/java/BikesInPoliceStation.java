import com.bounce.ops.utils.Utils;
import com.bounce.utils.DatabaseConnector;
import com.bounce.utils.Log;
import org.apache.log4j.Logger;
import org.jooq.Record;
import org.jooq.Result;

public class BikesInPoliceStation {

    private Result<Record> records;

    private final static Logger logger = Log.getLogger(BikesInPoliceStation.class.getName());

    public Result<Record> getRecords() {
        return records;
    }

    public boolean  fetchRecords() {
        try {
            String sqlQuery = "select b.id, b.lat,b.lon " +
                    "from parking p " +
                    "inner join bike b " +
                    "on b.geo_id = p.geo_id " +
                    "where b.geo_id=2 and p.category_id=5 " +
                    "and status in ('idle', 'oos') and ST_DWithin(p.fence::geography, ST_MakePoint(b.lon,b.lat)::geography, 0)";
            System.out.println("hyderabad bikes in police station query " + sqlQuery);
            Result<Record> records = DatabaseConnector.getDb().getReadDb1Connector().fetch(sqlQuery);
            if (records.size() != 0) {
                this.records=records;
            }
            return records.size() != 0;
        } catch (Exception e) {
                Utils.logError(e);
        }
        return false;
    }
}

