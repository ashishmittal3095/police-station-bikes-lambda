import com.amazonaws.services.lambda.runtime.Context;
import com.bounce.ops.utils.Constants;
import com.bounce.ops.utils.LocalConfigData;
import com.bounce.ops.utils.SNSGenericRequest;
import com.bounce.ops.utils.Utils;
import com.bounce.utils.Log;
import org.apache.log4j.Logger;
import org.jooq.Record;
import org.json.JSONObject;

import java.util.Map;

public class LambdaMethodHandler {
    private final static Logger logger = Log.getLogger(LambdaMethodHandler.class.getName());


    public String handleRequest(Map<String,Object> input, Context context) {
        LocalConfigData.getConfig();
        try {
            BikesInPoliceStation bikesInPoliceStation = new BikesInPoliceStation();
            if (bikesInPoliceStation.fetchRecords()) {
                for (Record record : bikesInPoliceStation.getRecords()) {
                    try {
                        System.out.println("record : " + record.toString());
                        String bikeId = record.get("id").toString();
                        String lat = record.get(Constants.LAT).toString();
                        String lon = record.get(Constants.LON).toString();
                        System.out.println("Bike Id : " + bikeId + " Lat : " + lat + " Lon : " + lon);
                        int teamId = Utils.getTeamId(2,lat,
                                lon,
                                Constants.STATEMENT_TEAM_MAPPING.get(Constants.BIKE_IN_POLICE_STATION.toLowerCase()));
                        System.out.println("Team id" + teamId);
                        JSONObject createTaskJsonData = this.getCronTaskJSON(Constants.BIKE_IS_IN_POLICE_STATION, teamId, bikeId,Constants.OOS_CRON);
                        System.out.println(createTaskJsonData.toString());
                        SNSGenericRequest.publishToTopic(Constants.TASK_EVENTS, createTaskJsonData);
                    }
                    catch (Exception e){
                        Utils.logError(e);
                    }
                }
            } else {
                logger.info("No bikes found in police station");
            }
        } catch (Exception e) {
            Utils.logError(e);
        }

        return "Service job Police station task complete";
    }

    private JSONObject getCronTaskJSON(String iTaskTypeName, int iClusterId, String bikeId, String cron) {
        JSONObject data = new JSONObject();
        data.put("task_type", iTaskTypeName);
        data.put("event_name", "create_task");
        data.put("source", cron);
        data.put("cluster_id", iClusterId);
        data.put("bike_id", bikeId);
        data.put("hawkeye_task", true);
        JSONObject taskEvent = new JSONObject();
        taskEvent.put("topic", Constants.TASK_EVENTS);
        taskEvent.put("data", data);
        logger.info("create task json from cron job data for bike id : " + bikeId + " data : " + taskEvent.toString());
        return taskEvent;
    }
}